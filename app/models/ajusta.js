exports.definition = {
    config: {
	columns: {
	    "lingua": "String",
	    "xerca": "List",
	    "id": "Number"
	},
	defaults: {
	    "id": 0
	},
	adapter: {
	    type: "properties",
	    collection_name: "ajusta",
	    idAttribute: "id"
	}
    },
    extendModel: function(Model) {
	_.extend(Model.prototype, {
	    // extended functions and properties go here
	});

	return Model;
    },
    extendCollection: function(Collection) {
	_.extend(Collection.prototype, {
	    // extended functions and properties go here
	});

	return Collection;
    }
};
