$.nota.url = "/introdui/fundal.html";

function doAbri() {
    if(Ti.Platform.name=="android") {
        $.fundal.activity.actionBar.displayHomeAsUp = true;
        $.fundal.activity.actionBar.onHomeIconItemSelected = doClui;
    }
}

function doClui() {
    $.fundal.close();
}

$.nota.enableZoomControls = false;

var args = arguments[0] || {};
