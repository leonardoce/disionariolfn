var args = arguments[0] || {};

function doAbri() {
    if(Ti.Platform.name=="android") {
        $.introdui.activity.actionBar.displayHomeAsUp = true;
        $.introdui.activity.actionBar.onHomeIconItemSelected = doClui;
    }
}

function doClui() {
    $.introdui.close();
}

$.testo.enableZoomControls = false;
$.testo.url = "/introdui/introdui.html";
