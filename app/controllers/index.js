MAX_RESULTS = 100;

function doSearchBarClick() {
    // De la documentos de Titanium
    // if (Ti.Platform.name === 'android') {
    //     // Clear search bar
    //     $.searchBar.value ="";
    //     // hiding and showing the search bar forces it back to its non-focused appearance.
    //     $.searchBar.hide();
    //     $.searchBar.show();
    // }

    xerca($.searchBar.value);
}

function doSearchBarCancel() {
    $.searchBar.value="";
}

function doAjustaLingua() {
    var opts = [
	"Elefel",
	"EO -> LFN",
	"EN -> LFN",
	"FR -> LFN"
    ];

    var dialog = Ti.UI.createOptionDialog({ options:opts,
					    cancel:0,
					    selectedIndex:0,
					    title: "Lingua?" });
    dialog.addEventListener("click", function(e) {
	if(e.index==0) {
	    doElefen();
	} else if(e.index==1) {
	    doEsperanto();
	} else if(e.index==2) {
	    doEngles();
	} else if(e.index==3) {
	    doFranses();
	}
	dialog = null;
    });
    dialog.show();
    
}

function xerca(s) {
    var disionario = require("disionario").disionario;
    var i, results;
    var report;

    var lingua = getLingua();

    if(!s) return;

    $.activity.show();

    s = s.toLowerCase();
    results = 0;
    report = "<html><body>";

    for(i=0; i<disionario.length; i++) {
	var key = disionario[i][lingua];
	if(key && key.indexOf(s)>(-1)) {
	    report += parolaAHtml(disionario[i]);
	    report += "<hr>"

	    results++;
	    if(results>MAX_RESULTS) break;
	}
    }

    $.activity.hide();

    report += "</body></html>"
    $.report.html = report;
}

function parolaAHtml(word) {
    var result = "";
    var i;

    // Nome da parola e cat
    result += "<b>" + word.lfn + "</b> ";
    if(word.cat) {
	result += "<i>";
	for(i=0; i<word.cat.length; i++) {
	    result += word.cat[i] + " ";
	}
	result += "</i>";
    }
    result += "<br>";

    // Defini
    if(word.defini) {
	result += "<span style='color:blue'>&nbsp;&nbsp;" + word.defini + "</span>";
	result += "<br>";
    }

    // Tason
    if(word.tason) {
	result += "<i style='color:blue'>&nbsp;&nbsp;" + word.tason + "</i>";
    }

    // EN
    if(word.en) {
	result += "&nbsp;&nbsp;EN: " + word.en + "<br>";
    }
    // EO
    if(word.eo) {
	result += "&nbsp;&nbsp;EO: " + word.eo + "<br>";
    }
    // FR
    if(word.fr) {
	result += "&nbsp;&nbsp;FR: " + word.fr + "<br>";
    }

    return result;
}

function doCreateOptionsMenu(e) {
    e.menu.add({title:"Introdui a elefen",
		icon: Ti.Android.R.drawable.ic_menu_search,
		showAsAction: Ti.Android.SHOW_AS_ACTION_NEVER})
	.addEventListener("click", doIntrodui);

    e.menu.add({title:"Vocabulo fundal",
		icon: Ti.Android.R.drawable.ic_menu_search,
		showAsAction: Ti.Android.SHOW_AS_ACTION_NEVER})
	.addEventListener("click", doFundal);
}

function doWindowOpen() {
    if (Ti.Platform.name === 'android') {
	$.index.activity.onCreateOptionsMenu = doCreateOptionsMenu;
    }
}

// -------------
// Cambia lingua
// -------------

function doElefen() {
    var ajusta = Alloy.Models.instance('ajusta');
    ajusta.set('lingua', 'lfn');
    ajusta.save();
    $.btAjustaLingua.title='Elefen';
    $.index.title = 'Disionario: Elefen -> Otras';

    xerca($.searchBar.value);
}

function doEsperanto() {
    var ajusta = Alloy.Models.instance('ajusta');
    ajusta.set('lingua', 'eo');
    ajusta.save();
    $.btAjustaLingua.title='Esperanto';
    $.index.title = 'Disionario: Esperanto -> Elefen';

    xerca($.searchBar.value);
}

function doEngles() {
    var ajusta = Alloy.Models.instance('ajusta');
    ajusta.set('lingua', 'en');
    ajusta.save();
    $.index.title = 'Disionario: Engles -> Elefen';
    $.btAjustaLingua.title='Engles';

    xerca($.searchBar.value);
}

function doFranses() {
    var ajusta = Alloy.Models.instance('ajusta');
    ajusta.set('lingua', 'fr');
    ajusta.save();
    $.index.title = 'Disionario: Franses -> Elefen';
    $.btAjustaLingua.title='Franses';

    xerca($.searchBar.value);
}

function doIntrodui() {
    Alloy.createController('introdui').getView().open();
}

function doFundal() {
    Alloy.createController('fundal').getView().open();
}

// -----------
// Persistence
// -----------
function getLingua() {
    var ajusta = Alloy.Models.instance('ajusta');
    if(!ajusta.get('lingua')) return "lfn";
    return ajusta.get('lingua');
}

// ----
// Init
// ----

function init() {
    var lingua=getLingua();
    if(getLingua()=='eo') {
	doEsperanto();
    } else if(getLingua()=='en') {
	doEngles();
    } else if(getLingua()=='fr') {
	doFranses();
    } else {
	doElefen();
    }
}

init();
$.index.open();
